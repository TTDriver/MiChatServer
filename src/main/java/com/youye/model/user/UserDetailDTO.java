package com.youye.model.user;

/**
 * **********************************************
 * <p/>
 * Date: 2018-09-14 09:56
 * <p/>
 * Author: SinPingWu
 * <p/>
 * Email: wuxinping@ubinavi.com.cn
 * <p/>
 * brief: 用户详细信息。
 * <p/>
 * history:
 * <p/>
 * **********************************************
 */
public class UserDetailDTO extends UserInfoDTO {

}
